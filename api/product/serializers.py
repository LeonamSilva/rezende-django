from rest_framework import serializers
from models import Product

class ProductSerializer(serializers.HyperlinkedModelSerializer):    
    class Meta:
        model = Product
        fields = ('code', 'name', 'description', 'barcode', 'base_price')
