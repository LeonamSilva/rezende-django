# -*- coding: utf-8 -*-
from django.conf.urls import url
import views

urlpatterns = [
    url(r'^', views.ProductList.as_view()),
    url(r'^(?P<code>[0-9]+)/$', views.ProductDetail.as_view()),
]