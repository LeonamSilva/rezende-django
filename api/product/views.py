# -*- coding: utf-8 -*-
from serializers import ProductSerializer
from models import Product
from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, mixins, generics

class ProductList(mixins.ListModelMixin, mixins.CreateModelMixin,generics.GenericAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
        

class ProductDetail(APIView):

    def get_object(self, code):
        try:
            return Product.objects.get(code=code)
        except Product.DoesNotExist:
            raise Http404

    def get(self, req, code, format=None):
        product = self.get_object(code)
        serializer = ProductSerializer(product)
        return Response(serializer.data)

    def put(self, req, code, format=None):
        product = self.get_object(code)
        serializer = ProductSerializer(product, data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, req, code, format=None):
        product = self.get_object(code)
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)