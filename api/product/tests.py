from product.models import Product
from django.test import TestCase

class ProductTest(TestCase):
    def setUp(self):
        product = Product(name='Copo de Cristal',base_price=100.50)
        product.save()

    def tearDown(self):
        pass

    def testCreatedObject(self):
        self.assertEquals(Product.objects.count(), 1)