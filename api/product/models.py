# -*- coding: utf-8 -*-
#from PIL import Image
from django.db import models
# import qrcode
import os.path
#from settings import IP_SERVER, MEDIA_ROOT


class Product(models.Model):
    code = models.AutoField(primary_key=True,unique=True)
    name = models.CharField(max_length=30,default='')
    description = models.CharField(max_length=200,default='',blank=True)
    barcode = models.CharField(max_length=20,default='',unique=True)
    base_price = models.DecimalField(max_digits=20,decimal_places=2,default=0)
    # available = models.BooleanField(default=False)
    #qr_code = models.ImageField(upload_to='qr_code',blank=True)

    # def generateQRCode(self):
    #     filename = '%(name)s.jpg' % dict(name=str(self.barcode))
    #     qrcode_dir = 'qrcode'
    #     path = os.path.join(MEDIA_ROOT, qrcode_dir, filename)

    #     #Generate .jpg
    #     img = qrcode.make(self.view_url)
    #     #img._img = img._img.resize((219, 177), Image.ANTIALIAS)
    #     img.save(path)

    #     #Save in the model
    #     self.qr_code = os.path.join(qrcode_dir, filename)
    #     self.save()

    # def getImages(self):
    #     return ProductImage.objects.filter(product=self)

    # @classmethod
    # def create(cls, values, files):
    #     product = cls(name = values.get('product_name'),
    #                                           description = values.get('product_description'),
    #                                           barcode = values.get('product_barcode'),
    #                                           # available=post.get('product_barcode'),
    #                                           base_price = float(values.get('product_price').replace(',',''))
    #     )
    #     product.save()
    #     #product.generateQRCode()


    #     for upfile in files.values():
    #         new_image = ProductImage(product=product)
    #         new_image.image.save(upfile.name,upfile)
    #         new_image.save()

    #     Product.fill_images(MAX_IMAGES-len(files), product)
    #     return product

    # @classmethod
    # def update(cls,values, files):
    #     product = cls.objects.get(barcode=values.get('product_barcode'))

    #     product.name = values.get('product_name')
    #     product.description = values.get('product_description')
    #     product.barcode = values.get('product_barcode')
    #     # product.available = post.get('product_available')
    #     product.base_price = float(values.get('product_price').replace(',',''))
    #     product.save()
    #     # product.generateQRCode()

    #     # Uploaded Files
    #     images = [image for image in product.getImages() if not hasattr(image.image,'url')]
    #     upfiles = [up for up in files.values()]

    #     #Take Only empty images and just assing to a upped file
    #     for upfile in upfiles:
    #         image = images.pop()
    #         image.image.save(upfile.name, upfile)
    #         image.save()
    #     return product

    # @classmethod
    # def fill_images(cls, last, product):
    #     for i in range(0,last):
    #         image = ProductImage(product=product)
    #         image.save()

    # @property
    # def view_url(self):
    #     return 'http://%(ip_server)s/view/%(barcode)s' % dict(ip_server=IP_SERVER,barcode=self.barcode)



    def __unicode__(self):
        return u'%s' % (self.name)

MAX_IMAGES = 3

# class ProductImage(models.Model):
#     product = models.ForeignKey(Product)
#     image = models.ImageField(upload_to='photos',blank=True)

#     def __unicode__(self):
#         return u'%s' % (self.image)



