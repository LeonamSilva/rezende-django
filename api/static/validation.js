$('document').ready(function(){
    $('#product_name').blur(function(){
        if ($(this).val().length == 0){
            alert('Campo Nome Vazio');
            $(this).focus();
        }
    });

    $('#product_price').blur(function(){
        if ($(this).val().length == 0){
            alert('Campo Preço Vazio');
            $(this).focus();
        }
    });

    $("#product_price").keypress(function (e) {
        if (! String.fromCharCode(e.keyCode).match(/[0123456789.,]/g)){
            alert('Campo Preço so aceita Numeros');
            $(this).attr('value','');
            $(this).focus();
        }
    });

    $('#product_description').blur(function(){
        if ($(this).val().length == 0){
            alert('Campo Descrição Vazio');
            $(this).focus();
        }
    });

    $('#product_barcode').blur(function(){
        if ($(this).val().length == 0){
            alert('Campo Codigo de Barras Vazio');
            $(this).focus();
        }
            Dajaxice.ajax.checkBarcode(function callback(json){
                                                if (json.message == 'True'){
                                                        alert("Este Codigo de Barras ja Existe no Banco de Dados");
                                                        $(this).attr('value','');
                                                        $(this).focus();
                                          }},
                {'barcode':$(this).val()}
            );


    });

    $("#product_barcode").keypress(function (e) {
        if (! String.fromCharCode(e.keyCode).match(/[0123456789]/g)){
            alert('Codigo de Barras so aceita Numeros');
            $(this).attr('value','');
            $(this).focus();
        }
    });

 });



