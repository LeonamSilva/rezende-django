# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.conf.urls.static import static
import settings.dev as settings

urlpatterns = [
    url(r'^product/', include('product.urls')),
    url(r'^upload/', include('upload_file.urls'))
]

if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


