# -*- coding: utf-8 -*-
import os.path
from os import getenv
#from django.core.urlresolvers import reverse_lazy

PROJECT_DIR = os.path.dirname(os.pardir)
ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

# Este Numero é usado para referenciar o 'server' na leitura dos qr-codes
# IP_SERVER = '192.168.2.15:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': getenv('PG_DATABASE'),                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': getenv('PG_USER'),
        'PASSWORD': getenv('PG_PASSWD'),
        'HOST': getenv('PG_HOST'),                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': getenv('PG_PORT'),                      # Set to empty string for default.
    }
}
# LOGIN_URL = reverse_lazy('rezende_login')

TIME_ZONE = 'America/Belem'

LANGUAGE_CODE = 'pt-BR'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_DIR,'media')

MEDIA_URL = 'media/'

# STATIC_ROOT = ''

# STATIC_URL = '/static/'

# STATICFILES_DIRS = (
#     os.path.join(PROJECT_DIR, 'static'),
#     os.path.join(PROJECT_DIR, 'template'),
#     os.path.join(PROJECT_DIR, 'product', 'template'),
#     os.path.join(PROJECT_DIR, 'login', 'template')
# )


# STATICFILES_FINDERS = (
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
# #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
#     'dajaxice.finders.DajaxiceFinder'
# )

SECRET_KEY = 'tgq+5nz0y9_7v%1ws-ffuofp+9352zc^*ju_#tbg@u0z-m@1f-'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.middleware.transaction.TransactionMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

TEMPLATE_DIRS = (os.path.join(PROJECT_DIR, 'template'),
                 os.path.join(PROJECT_DIR, 'product', 'template'),
                 os.path.join(PROJECT_DIR, 'login', 'template')
)

DEFAULT_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    # 'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
]

LOCAL_APPS = [
    'product',
    'upload_file'
]

THIRD_PARTY_APPS = [
    'rest_framework'
]

INSTALLED_APPS = LOCAL_APPS + THIRD_PARTY_APPS + DEFAULT_APPS

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
}
